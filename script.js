(function(){

	// create a module
	var app = angular.module('websiteApp', ['ngRoute']);

	// configure routing
	app.config(function($routeProvider, $locationProvider){
		$routeProvider

			/* PUBLIC PAGES */
			// route for the home page
			.when('/', {
				templateUrl : 'pages/home.html',
                controller  : 'homeController'
			})

			// route for about page
			.when('/about',{
				templateUrl : 'pages/about.html',
                controller  : 'aboutController'
			})

			// route for the contact page, 
			.when('/contact', {
				templateUrl : 'pages/contact.html',
                controller  : 'contactController'
			})

			// route for dashboard page
			.when('/how-it-works',{
				templateUrl : 'pages/how-it-works.html',
				controller : 'howItWorksController'
			})

			// route for dashboard page
			.when('/tasks',{
				templateUrl : 'pages/task-browse.html',
				controller : 'taskController'
			})

			/* SECURE PAGES */
			// route for dashboard page
			.when('/dashboard',{
				templateUrl : 'pages/dashboard.html',
				controller : 'dashboardController',
				access: {
		            requiresLogin: true
		        }	
			})

			// route for non-existing route pages
			.otherwise({
	            redirectTo: '/'
	        })

			;

		// enable browser back to remove # on url
		$locationProvider.html5Mode(true);
	});

	// middleware - check for secured routes and check if the user can access it
	app.run(function($rootScope, $location){
		
		$rootScope.$on("$routeChangeStart", function (event, next) {
		  	
		  	// check if the page is secure
			if( next.access !== undefined ){

				// if the user does not have authorization to access the page
				
				// redirect user to the homepage 
				$location.path('/');
			}  	
		});
	});

	// home page controller
	app.controller('homeController', function($scope){
		$scope.message = 'home page';
	});

	// about page controller
	app.controller('aboutController', function($scope){
		$scope.message = 'about page';
	});

	// contact page controller
	app.controller('contactController', function($scope){
		$scope.message = 'contact page';
	});

	// dashboard controller
	app.controller('dashboardController', function($scope){

	});

	// how it works controller
	app.controller('howItWorksController', function($scope){

	});

	// task controller - browse, details, offer
	app.controller('taskController', function($scope){

	});

	// header controller
	app.controller('headerController', function($scope){
		$scope.loggedIn = false;
	});
	
})();